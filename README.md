**JHG Agent Code Intro:**

Included are some scripts and utilities for connecting an ai agent to the the junior high game as well as a server simulator for testing agents against other agents without an online connection.
Everything was build and intended for command line/terminal use.

**Simulator Explanation:**

The simulator code is made up of agentsimulator.py and serversimulator.py
though you never will nor should edit those files, instead you'll be creating an agent similar to what is found in basicagent.py
Within basicagent.py you'll find a class factory function basic_agent_factory, this will generate an agent class for either a connection to juniorhighgame.com or the agent simulator based on the is_simulated argument.

To test connecting the agent to juniorhighgame.com you run the associated agent's file, e.g. you'll be able to test the basic agent by running ``basicagent.py -c {jhg game code}`` from your command line with game code from the game lobby screen.

To create a simulator agent use the factory method with the argument is_simulated set to true this creates and returns a class that extends AgentSimulator and is the simulator version of the agent, 
Usage for the simulator can be round in test_code.py along with a random agent and a number of other sample agents setup to run a sample game. 
You'll test your simulator version by running ``test_code.py`` along with any parameters you want for the game settings (``test_code.py --help`` for usage)
I added some hastely put together visualizations of the graphs (samples in this directory) so you can review a simulation afterwards if you want, to see these graphs use the -v argument.

When building your own agent you'll create a class factory function like in the other agent files and change the play_round and new_message functions as you see fit. Then when using the agent in the simulator use the factory function with the argument is_simulated as True to create the simulator version of the agent class for instantiating instances of you agent. Then for connection to the jhg website set is_simulated to False to create the online version of the class for use in instantiating the agent(s).

The simulator was setup so that it expects token submissions and messages in the same format as the server while also returning the round info and graphs in the same format as the server so you can easily vary your agent code and experiment without needing to connect to juniorhighgame.com, so if it works with the simulator it will work with juniorhighgame.com


**General Information:**

jhg agents are inherently multithreaded (both the online version and the simulator \[out of necessity]) so keep that in mind when building your agent.

Your bots will work with two main functions, play_round and new_message, these are called when either the jhg server or the simulator code starts a new round or notifies the agent of a new player message to the agent (this include the agent's own messages).
Both of these function run on separate threads, so they are not synchronous and can be running at the same time accessing the same memory, as such you may want to implement simple locking functions to keep separate threads from modifying the same memory.
One way to handle this and other quirks of multithreaded processing would be setting up a sleep schedule in play_round that processes a queue that new_message pushes the new messages to (new_message locking the queue when adding anything to it while play_round locks it when processing the queue) then when the round is nearly done or some other condition occurs the agent submits its token assignments for that round (round time is found in milliseconds in the round_data variable or in the variable round_time).


**Useful Functions:**

These would be get_line_graph, get_tornado_graph, get_network_graph, send_message and submit_tokens. The first three get the graph data structures, while the latter two send a message and submit your token assignments respectively.
Its important to remember that the server will verify the integrity of your token assignments, so make sure they are valid i.e. sum to at most 2*number of players (value of round_tokens), are all integers and match the form used by the basic agent, so you (sadly) can't steal 2000 tokens from everyone and win.
get_current_round is called when the next round begins just before play_round and fills in the appropriote data in round_data, so you shouldn't ever need to call it and can assume it was called updating the round variables (see below) and it will be called right before the play_round function.


**Useful Member Variables:**

>_round_data_: this contains information for the round (see graph_structs.txt for a sample version)

>_round_num_: the current round number

>_round_time_: the amount of time left in the round (in milliseconds)

>_round_tokens_: the number of tokens you can use in the current round

>_player_list_: a list of the names of all the players in the game

>_player_name_: your agent's name in game

>_agent_name_: the name you assign to the agent when instantiating it (revealed at the end of the game)

>_player2idx_ and _idx2player_: dictionaries that map a player name to their index and vice versa, useful if you want to convert data to matrices or vectors and back again, this is what is used for the matrix format of the tornado graph

>_chat_type_: this will tell you want kind of chat the game has set up (Default is no chat, Free is identified chat and Anonymous is unidentified chat)

anything with a leading underscore is meant to be private (python conventions and all that stuff) so don't rely on them as they may not be there between the simulator and the online agent base classes.


**Useful Info:**

You'll find sample output of the simulator (its not too exciting) and samples of the data structures for the various graphs in sample_output.txt and graph_structs.txt respectively.

There is a file called agentdecorators in agentutils that has a couple decorators that are useful for debugging, just place @agentdecorators.print_args on the line before a function definition and it'll print out bars before and after 
execution (noted in the print statement as START \{function name} and END \{function name}) along with the value of any arguments passed into the function.

Don't bother calling get_round_transactions, the endpoint it calls is not implemented on the live jhg yet so you'll get a get request error.

For the easiest time you may just want to modify the cs670agent.py function as that is setup and works both online and with the simulator, just change the play_round and new_message functions.

**Important Notes:**

>All scripts were written with and expect python 3

>Required packages and their version numbers are included in requirements.txt (dnspython is a requirement for requests but depending on your package manager you may have to install that separately)

>All the bots have an argument in their constructer call is_bot, ALWAYS set this to false, its a feature we are still building out and is not included in the live version of jhg, you can ignore bot_role and bot_reg_key. If you don't set is_bot to false the online version will get a post error but it won't stop it from running or playing the game.

>It goes without saying but DO NOT change anything in either the AgentSimulator or AgentConnection class as doing so may cause your agent to not connect correctly
