# import local files
from agentutils import jhgutils
from agentutils import agentdecorators
from agentutils.httpconnect import HTTPConnection

# import packages
import socketio
import numpy as np

class AgentConnection:
    def __init__(self, code, base_url='http://www.juniorhighgame.com', agent_name='ai', 
            is_player=True, is_bot=True, bot_role=None, bot_reg_key=None):
        self._sio = socketio.Client()
        self._setup_socket_events()

        self._base_url = base_url
        self._client_id = None
        
        self.game_code = code
        self.agent_name = agent_name # Name shown at game over

        self._is_player = is_player
        if self._is_player:
            self._player_role = jhgutils.game_roles['player']
        else:
            self._player_role = jhgutils.game_roles['observer']
        
        self.player_list = None
        self.player2idx = None
        self.idx2player = None

        self.player_name = 'Navi' # Name assigned by the server

        self.round_data = {}
        self.round_num = -1
        self.round_time = -1.0
        self.round_tokens = 0

        self.chat_type = 'Default'

        self._is_bot = is_bot
        self._bot_role = bot_role
        self._bot_reg_key = bot_reg_key
        if self._is_bot:
            if self._bot_role is None:
                self._bot_role = jhgutils.bot_roles['basic']
            if self._bot_reg_key is None:
                self._bot_reg_key = jhgutils.bot_api_keys['basic']

        self._sio.connect(self._base_url)
        self._http = HTTPConnection(self._base_url)
        self._join_game()

    def wait(self):
        try:
            self._sio.wait()
        except:
            self._sio.disconnect()


    def _setup_socket_events(self):
    
        @self._sio.on(jhgutils.socket_reserved_event['connect'])
        def connect():
            print('connection established')

        
        @self._sio.on(jhgutils.socket_reserved_event['disconnect'])
        def disconnect():
            print(f'{self.player_name} disconnected from server')

        @self._sio.on(jhgutils.socket_event['start_game'])
        def on_start_game():
            print("Game Start!")
            self._process_round()

        
        @self._sio.on(jhgutils.socket_event['game_over'])
        def on_game_over(data):
            print('Game Over!')
            self._sio.disconnect()

        @self._sio.on(jhgutils.socket_event['new_chat_message_client'])
        def on_new_chat_message(data):
            self.new_message(data)

        @self._sio.on(jhgutils.socket_event['end_round'])
        def on_end_round(data):
            self._process_round()


    def _join_game(self):
        post_params = {
            "code": self.game_code,
            "isPlayer": self._is_player,
            "playerExperience": "0 to 1 games played",
            "userName": self.agent_name
        }

        response_data = self._http.post_request(
            resource_url=jhgutils.api_resource_dict['join'], 
            post_params=post_params
        )

        self._client_id = response_data['lobby']['clientId']
        self.chat_type = response_data['lobby']['chatType']

        socket_data = {
            "code": self.game_code,
            "id": self._client_id,
            "role": self._player_role
        }

        self._sio.emit(jhgutils.socket_event['joined_game'], socket_data)

        if self._is_bot:
            self._reg_bot()


    def _reg_bot(self):
        print('Oath to Order')
        post_params = {
            "code": self.game_code,
            "registrationKey": self._bot_reg_key,
            "botRole": self._bot_role,
            "clientId": self._client_id
        }

        response_data = self._http.post_request(
            resource_url=jhgutils.api_resource_dict['reg_bot'], 
            post_params=post_params
        )

        print('Register response: {}'.format(response_data))

   

    def _get_missed_messages(self):
        socket_data = {
            "code": self.game_code,
            "numClientMessages": 0,
            "chatType": self.chat_type
        }

        self._sio.emit(socket_event['check_missed_chat_messages'], socket_data)
    

    def send_message(self, message):
        socket_data = {
            "message": {
                "playerName": self.player_name,
                "body": message
                },
            "chatType": self.chat_type
        }

        self._sio.emit(jhgutils.socket_event['new_chat_message_server'], socket_data)
    

    def get_current_round(self):
        get_params = {
            "code": self.game_code,
            "clientId": self._client_id
        }

        response_data = self._http.get_request(
            resource_url=jhgutils.api_resource_dict['round'], 
            get_params=get_params
        )

        self.round_num = response_data['roundInfo']['round']
        self.round_data[self.round_num] = response_data['roundInfo']
        self.round_tokens = self.round_data[self.round_num]['numTokens']
        self.player_name = self.round_data[self.round_num]['playerName']

        if self.player_list is None:
            exchanges_given = response_data['roundInfo']['exchanges']['given']
            
            self.player_list = [
                given_data['name'] for given_data in exchanges_given
            ]

            self.player2idx = {
                player: idx for idx, player in enumerate(self.player_list)
            }

            self.idx2player = {
                idx: player for idx, player in enumerate(self.player_list)
            }


    def get_round_transactions(self, round_num=None):
        get_params = {
            "code": self.game_code,
            "clientId": self._client_id,
            "round": round_num if round_num is not None else self.round_num - 1
        }

        response_data = self._http.get_request(
            resource_url=jhgutils.api_resource_dict['transactions'], 
            get_params=get_params
        )

        clean_data = self._clean_data(response_data, [])
        return clean_data
    

    def get_line_graph(self):
        raw_data = self._get_graph_data(
            jhgutils.graph_types['line']
        )

        return self._parse_line_graph(raw_data)


    def get_tornado_graph(self, round_num=None, matrix_format=False):
        raw_data = self._get_graph_data(
            jhgutils.graph_types['tornado'], 
            round_num
        )

        return self._parse_tornado_graph(raw_data, matrix_format)


    def get_network_graph(self, round_num=None):
        raw_data = self._get_graph_data(
            jhgutils.graph_types['network'], 
            round_num
        )

        return self._parse_network_graph(raw_data)


    def _get_graph_data(self, graph_type, round_num=None):
        if round_num is None:
            round_num = self.round_num

        get_params = {
            "code": self.game_code,
            "type": graph_type,
        }

        if graph_type != jhgutils.graph_types['line']:
            get_params["round"] = round_num
            resource_key = 'graph_round'
        else:
            resource_key = 'graph'

        response_data = self._http.get_request(
            resource_url=jhgutils.api_resource_dict[resource_key], 
            get_params=get_params
        )

        return response_data

    def _clean_data(self, data, remove_list):
        if isinstance(data, dict):
            for remove_key in remove_list:
                if remove_key in data.keys():
                    del data[remove_key]
                    
            for key in data.keys():
                self._clean_data(data[key], remove_list)
        elif isinstance(data, list):
            for el in data:
                self._clean_data(el, remove_list)


    def _parse_tornado_graph(self, tornado_data, matrix_format=False):
        self._clean_data(
            tornado_data['graph']['gameData'][0]['roundData'], 
            ['_id']
        )

        cleaned_data = tornado_data['graph']['gameData'][0]['roundData']
        
        if matrix_format:
            num_players = len(self.player_list)
            tornado_mat = np.zeros((num_players, num_players))

            for player_data in cleaned_data:
                giver_idx = self.player2idx[player_data['playerName']]
                for interaction in player_data['playerData']:
                    reciever_idx = self.player2idx[interaction['name']] 
                    tornado_mat[giver_idx, reciever_idx] = interaction['value']

            return tornado_mat
        else:
            return cleaned_data


    def _parse_line_graph(self, line_data):
        self._clean_data(line_data['graph']['roundData'], ['_id'])
        return line_data['graph']['roundData']


    def _parse_network_graph(self, network_data):
        self._clean_data(
            network_data['graph']['networkData'][0],
            ['_id', 'roundNum']
        )
        return network_data['graph']['networkData'][0]


    def submit_tokens(self, transaction):
        post_params = {
            "code": self.game_code,
            "clientId": self._client_id,
            "round": self.round_num,
            "submission": {
                "tokenAssignments": transaction
            }
        }

        response_data = self._http.post_request(
            resource_url=jhgutils.api_resource_dict['submit'], 
            post_params=post_params
        )

    def _process_round(self):
        self.get_current_round()

        self.play_round()


    def play_round(self):
        if self.chat_type != jhgutils.chat_type['free']:
            self.send_message("2 tkns to everyone")
        elif self.chat_type != jhgutils.chat_type['anonymous']:
            self.send_message("2 tkns to everyone -{}".format(self.player_name))
        
        round_transaction = [
            {"receiverName": player, "amount": 2} for player in self.player_list
        ]
        self.submit_tokens(round_transaction)


    def new_message(self, message):
        print("{playerName} says '{body}'".format(**message))
