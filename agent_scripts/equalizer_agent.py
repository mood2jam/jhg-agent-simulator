# import local files
from agentconnection import AgentConnection
from agentsimulator import AgentSimulator
from agentutils import agentdecorators
from agentutils import jhgutils

# import packages
import argparse
import time
import numpy as np

def equalizer_agent_factory(is_simulated=False):
    ConnectionClass = None
    if is_simulated:
        AgentType = AgentSimulator
    else:
        AgentType = AgentConnection

    class EqualizerAgent(AgentType):
        def __init__(self, code, base_url='http://www.juniorhighgame.com', 
                agent_name='ai', is_player=True, is_bot=True, bot_role=None, 
                bot_reg_key=None, k_friends=5, k_enemies=5, generous_start=False, verbose=False):
            super().__init__(code, base_url, agent_name, is_player, 
                is_bot, bot_role, bot_reg_key)
            self.k_friends = k_friends
            self.k_enemies = k_enemies
            self.generous_start = generous_start
            self.verbose = verbose


        def init_vars(self):
            self.diff_pops = np.zeros(len(self.player_list))
            self.cur_pops = np.zeros(len(self.player_list))
            self.weighted_tokens_tracker = np.zeros(len(self.player_list))
            self.token_dist = np.zeros(len(self.player_list))
            self.k_friends_list = []
            self.k_enemies_list = []
            self.players_met = [i for i in range(len(self.player_list))]
            self.my_idx = self.player2idx[self.player_name]
            self.players_met.remove(self.my_idx)
            self.current_test = np.random.choice(self.players_met,2)



        def update_trackers(self):
            if self.round_num == 1:
                self.init_vars()
            # Get the current weighted tokens
            self.cur_weighted_tokens_tracker = np.zeros(len(self.player_list))
            cur_pops = self.round_data[self.round_num]['playerPopularities']

            # Update the popularity difference tracker
            for player_data in cur_pops:
                player_idx = self.player2idx[player_data['name']]
                # if player_data['name'] == self.player_name:
                #     self.my_idx = player_idx
                self.diff_pops[player_idx] = player_data['popularity'] - self.cur_pops[player_idx]
                self.cur_pops[player_idx] = player_data['popularity']

            for player_data in self.round_data[self.round_num]['exchanges']['received']:
                player_idx = self.player2idx[player_data['name']]
                self.weighted_tokens_tracker[player_idx] += player_data['amount'] * self.cur_pops[player_idx]
                self.cur_weighted_tokens_tracker[player_idx] += player_data['amount'] * self.cur_pops[player_idx]

            for player_data in self.round_data[self.round_num]['exchanges']['given']:
                player_idx = self.player2idx[player_data['name']]
                self.weighted_tokens_tracker[player_idx] -= player_data['amount'] * self.cur_pops[player_idx]
                self.cur_weighted_tokens_tracker[player_idx] -= player_data['amount'] * self.cur_pops[player_idx]

            # Now update my k_friends and k_enemies
            relationship_order = np.argsort(self.weighted_tokens_tracker)
            self.k_enemies_list = relationship_order[:self.k_enemies]
            self.k_friends_list = relationship_order[::-1][:self.k_friends]
            self.token_dist = np.zeros(len(self.player_list))




            if self.verbose:
                print("Updating everything...")
                print("Weighted tokens:", self.weighted_tokens_tracker)
                print("Cur weighted tokens", self.cur_weighted_tokens_tracker)
                print("Best friends are:", self.k_friends_list)
                print("Enemies are:", self.k_enemies_list)
                print("Current test is:", self.current_test)
                print("My index is:", self.my_idx)
                print()


        def play_round(self):

            # DONE Initialization: find players that will be good allies. Hold onto tokens first round? Or randomly give out to k people?
            # DONE 1. Calculate the difference between current popularity and previous popularity. If you are negative, focus
            # energy on retaliation, if I cannot effectively retaliate, hold onto all tokens
            # DONE 2. If I am not negative, then look at all tokens given and received. Focus on replaying top k friends and
            # then attack top p enemies.
            # DONE 3. If I have tokens left over, use them to attack the enemies of my friends. This is done by looking at
            # which of my friends are negative and multiply that by the number of tokens given to me, and then by the
            # percentage that other players are attacking them. Steal tokens from those players
            # DONE 4. Keep the rest of the tokens

            self.update_trackers()
            round_transaction = list()

            if self.round_num == 1:
                # Keep tokens on the first round

                if not self.generous_start:
                    tokens_to_give = self.round_tokens // 4
                    tokens_to_keep = self.round_tokens - tokens_to_give*2
                    for player in self.player_list:
                        player_idx = self.player2idx[player]
                        if player == self.player_name:
                            self.my_idx = self.player2idx[player]
                            round_transaction.append({
                                "receiverName": player,
                                "amount": tokens_to_keep
                            })
                        elif player_idx in self.current_test:
                            round_transaction.append({
                                "receiverName": player,
                                "amount": tokens_to_give
                            })
                        else:
                            round_transaction.append({
                                "receiverName": player,
                                "amount": 0
                            })

                    if self.current_test is not None:
                        amount = 0
                        for test_idx in self.current_test:
                            if self.token_dist[test_idx] >= 0:
                                amount =  tokens_to_give
                                self.weighted_tokens_tracker[test_idx] += amount * self.cur_pops[self.my_idx]

                            if amount != 0:
                                # If we were able to interact with them somehow, test another player
                                self.current_test = np.random.choice(self.players_met, 2)

                # Or give it out to random people
                else:
                    friends_list = np.random.choice(self.player_list, self.k_friends)
                    tokens_to_distribute = (self.round_tokens // 2) // self.k_friends # Keep at least half the tokens
                    tokens_to_keep = self.round_tokens - tokens_to_distribute * self.k_friends

                    for player in self.player_list:
                        if player == self.player_name:
                            self.my_idx = self.player2idx[player]
                            round_transaction.append({
                                "receiverName": player,
                                "amount": tokens_to_keep
                            })
                        elif player in friends_list:
                            round_transaction.append({
                                "receiverName": player,
                                "amount": tokens_to_distribute
                            })
                        else:
                            round_transaction.append({
                                "receiverName": player,
                                "amount": 0
                            })

            else:
                if self.verbose:
                    print("My popularity change:", self.diff_pops[self.my_idx])
                    print()
                if self.diff_pops[self.my_idx] <= 0:
                    # I am losing popularity, go into retaliation/protection mode
                    enemy_indices = np.where(self.cur_weighted_tokens_tracker < 0)[0]
                    total_to_recouperate = np.abs(self.cur_weighted_tokens_tracker[enemy_indices]).sum()
                    tokens_left = self.round_tokens
                    if total_to_recouperate >= tokens_left * self.cur_pops[self.my_idx]:
                        if self.verbose:
                            print("In protection mode (holding onto tokens)")
                        # This means we will not be able to retaliate effectively, so keep all of our tokens to protect ourselves
                        self.token_dist[self.my_idx] = tokens_left
                    else:
                        if self.verbose:
                            print("Attacking all the mean people in retribution mode.")
                        for enemy_idx in enemy_indices:
                            retribution = int(self.cur_weighted_tokens_tracker[enemy_idx] / self.cur_pops[self.my_idx])

                            if np.abs(retribution) > tokens_left:
                                retribution = -tokens_left

                            tokens_left -= np.abs(retribution)

                            self.token_dist[enemy_idx] += retribution

                            # Updated the weighted tokens tracker to track our relationship
                            # self.weighted_tokens_tracker[enemy_idx] -= retribution * self.cur_pops[self.my_idx]
                            if self.verbose:
                                print("Taking {0} from player {1} (total of {2}).".format(retribution, enemy_idx,
                                                                                        self.token_dist[enemy_idx] *
                                                                                        self.cur_pops[self.my_idx]))



                        # Make sure we at least keep some of them
                        self.token_dist[self.my_idx] += tokens_left // 2

                        # Update the tokens left
                        tokens_left -= tokens_left // 2
                        tokens_left = self.give_to_friends(tokens_left)
                        tokens_left = self.take_from_enemies(tokens_left)
                        tokens_left = self.punish_enemies_of_friends(tokens_left)

                        if self.current_test is not None:
                            amount = 0
                            for test_idx in self.current_test:
                                if self.token_dist[test_idx] >= 0:
                                    amount = np.min([tokens_left // 2, self.round_tokens // 3])
                                    self.weighted_tokens_tracker[test_idx] += amount * self.cur_pops[
                                        self.my_idx]

                                if amount != 0:
                                    # If we were able to interact with them somehow, test another player
                                    self.current_test = np.random.choice(self.players_met, 2)

                        # CHANGE THIS IF NEEDED
                        self.token_dist = self.token_dist*2 // 3
                        # Make sure we at least keep some of them
                        self.token_dist[self.my_idx] += (self.round_tokens - np.abs(self.token_dist).sum())
                else:
                    tokens_left = self.round_tokens
                    # print("Tokens left 1", tokens_left)
                    # print()
                    tokens_left = self.give_to_friends(tokens_left)
                    # print("Tokens left 2", tokens_left)
                    # print()
                    tokens_left = self.take_from_enemies(tokens_left)
                    # print("Tokens left 3", tokens_left)
                    # print()
                    tokens_left = self.punish_enemies_of_friends(tokens_left)
                    # print("Tokens left 4", tokens_left)
                    # print()


                    if self.current_test is not None:
                        amount = 0
                        for test_idx in self.current_test:
                            if self.token_dist[test_idx] >= 0:
                                amount = np.min([tokens_left // 2, self.round_tokens//3])
                                self.weighted_tokens_tracker[test_idx] += amount * self.cur_pops[
                                    self.my_idx]

                            if amount != 0:
                                # If we were able to interact with them somehow, test another player
                                self.current_test = np.random.choice(self.players_met, 2)

                    # CHANGE THIS IF NEEDED
                    self.token_dist = self.token_dist*2 // 3
                    # Make sure we at least keep some of them
                    self.token_dist[self.my_idx] += (self.round_tokens - np.abs(self.token_dist).sum())

                round_transaction = [
                    {
                        "receiverName": player,
                        "amount": self.token_dist[self.player2idx[player]]
                    } for player in self.player_list
                ]

            if self.verbose:
                print("Final token distribution...")
                print(self.token_dist)
                print(round_transaction)
                print("Round tokens", self.round_tokens)
            self.submit_tokens(round_transaction)

        def give_to_friends(self, tokens_left):
            if self.verbose:
                print("Giving to friends...")

            for idx in self.k_friends_list:
                if tokens_left == 0 or self.weighted_tokens_tracker[idx] <= 0 or idx == self.my_idx:
                    # If our friends are actually attacking us, don't help them.
                    break
                # Be generous in gifts
                gift = np.min([int(self.weighted_tokens_tracker[idx] / self.cur_pops[self.my_idx]), self.round_tokens//2])
                assert gift >= 0
                if gift > tokens_left:
                    gift = tokens_left

                self.token_dist[idx] += gift

                if self.verbose:
                    print("Giving {0} to player {1} (total of {2}).".format(gift, idx, gift * self.cur_pops[self.my_idx]))
                # Updated the weighted tokens tracker to track our relationship
                # self.weighted_tokens_tracker[idx] -= self.token_dist[idx] * self.cur_pops[self.my_idx]
                tokens_left -= gift
            return tokens_left

        def take_from_enemies(self, tokens_left):
            if self.verbose:
                print("Attacking my enemies...")

            for idx in self.k_enemies_list:
                if tokens_left == 0 or self.weighted_tokens_tracker[idx] >= 0 or idx == self.my_idx:
                    # If they are not taking from us, no need to attack them
                    break
                retribution = int(self.weighted_tokens_tracker[idx] / self.cur_pops[self.my_idx])
                assert retribution <= 0
                if np.abs(retribution) > tokens_left:
                    retribution = -tokens_left
                self.token_dist[idx] += retribution
                if self.verbose:
                    print("Taking {0} from player {1} (total of {2}).".format(retribution, idx, retribution * self.cur_pops[self.my_idx]))
                # Updated the weighted tokens tracker to track our relationship
                # self.weighted_tokens_tracker[idx] -= retribution * self.cur_pops[self.my_idx]
                tokens_left -= np.abs(retribution)
            return tokens_left

        def punish_enemies_of_friends(self, tokens_left):

            tornado_matrix = self.get_tornado_graph(matrix_format=True)
            for idx in self.k_friends_list:
                if tokens_left == 0 or self.cur_weighted_tokens_tracker[idx] <= 0 or idx == self.my_idx:
                    break
                if self.diff_pops[idx] < 0:
                    if self.verbose:
                        print("Punishing the enemies of my friends...")
                    friend_vector = tornado_matrix[:,idx]
                    if self.verbose:
                        print("Friend vector:", friend_vector)
                        # print(np.round(tornado_matrix, 1))
                        # print("Friend idx", idx)
                    # Our friend is losing popularity, so we will help them
                    perc_decrease = self.diff_pops[idx] / self.cur_pops[idx]
                    effective_tokens_lost = int( perc_decrease * self.cur_weighted_tokens_tracker[idx] / self.cur_pops[self.my_idx])
                    enemies_of_my_friend = np.where(friend_vector < 0)[0]

                    if self.verbose:
                        print("Effective tokens lost:", effective_tokens_lost)

                    # Assign the blame
                    norm_enemies_of_my_friend = np.zeros(len(self.player_list))
                    norm_enemies_of_my_friend[enemies_of_my_friend] = np.abs(friend_vector[enemies_of_my_friend])
                    # print("DEBUG", enemies_of_my_friend, friend_vector, np.abs(friend_vector[enemies_of_my_friend]))
                    if np.sum(norm_enemies_of_my_friend) > 0:
                        norm_enemies_of_my_friend = norm_enemies_of_my_friend / np.sum(norm_enemies_of_my_friend)

                    # Now calculate how many tokens to steal
                    norm_enemies_of_my_friend *= effective_tokens_lost
                    norm_enemies_of_my_friend.astype(np.int32)

                    for enemy_idx in enemies_of_my_friend:
                        if tokens_left <= 0 and enemy_idx != self.my_idx:
                            break
                        retribution = np.min([-2, norm_enemies_of_my_friend[enemy_idx]])
                        assert retribution <= 0
                        if np.abs(retribution) > tokens_left:
                            retribution = -tokens_left

                        self.token_dist[enemy_idx] += retribution
                        # Updated the weighted tokens tracker to track our relationship
                        # self.weighted_tokens_tracker[idx] -= self.token_dist[enemy_idx] * self.cur_pops[self.my_idx]
                        if self.verbose:
                            print("Taking {0} from player {1} (total of {2})".format(retribution, enemy_idx, retribution * self.cur_pops[self.my_idx]))
                        tokens_left -= np.abs(retribution)

            return tokens_left

        
        def new_message(self, message):
            # This will be triggered by an event and thus in a different
            # thread from play_round but it shares access to the memory
            # message is a dictionary with the sender name as 'playerName'
            # and the message as 'body'
            # TODO: Change this for how you want to handle messages
            print("{playerName} says '{body}'".format(**message))

    return EqualizerAgent

        
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c", "--code", type=str, required=True, 
        help="code for the game to join"
    )

    args = parser.parse_args()

    Agent670Online = cs670_agent_factory(is_simulated=False)

    bot = Agent670Online(args.code, base_url='http://www.juniorhighgame.com', 
        agent_name='ai670', is_player=True, is_bot=False)
    
    bot.wait()