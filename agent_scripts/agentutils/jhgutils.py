'''This file contains dictionaries for mapping to the correct string that the
    jhg server expects. Some will appear redundant but they should be used for 
    consistency and in case of changes'''

api_resource_dict = {
    "join": "/api/join",
    "round": "/api/round/current",
    "graph_round": "/api/graph/round",
    "graph": "/api/graph/",
    "submit": "/api/submit",
    "reg_bot": "/bot/register",
    "transactions": "/bot/data/transactions",
}

game_roles = {
    "player": "play",
    "observer": "observe"
}

graph_types = {
    "tornado": "tornado", 
    "line": "line", 
    "network": "network"
}

chat_type = {
    "default": "Default",
    "free": "Free",
    "anonymous": "Anonymous"
}

socket_event = {
    "register": "register",
    "is_registered": "isRegistered",
    "register_failed": "registerFailed",
    "joined_game": "joinedGame",
    "lobby_update": "lobbyUpdate",
    "lobby_failure": "lobbyFailure",
    "exit_lobby": "exitLobby",
    "is_new_admin": "isNewAdmin",
    "start_game": "startGame",
    "end_round": "endRound",
    "game_over": "gameOver",
    "new_chat_message_client": "newChatMessage",
    "update_chat_messages": "updateChatMessages",
    "new_chat_message_server": "newChatMessageServer",
    "check_missed_chat_messages": "checkMissedChatMessages"
}

socket_reserved_event = {
    "connect": "connect",
    "connect_timeout": "connect_timeout",
    "connect_error": "connect_error",
    "reconnect": "reconnect",
    "reconnect_error": "reconnect_error",
    "reconnect_failed": "reconnect_failed",
    "disconnect": "disconnect",
}

bot_roles = {
    "basic": "basic"
}

bot_api_keys = {
    "basic": "I solemnly pledge my allegiance to the Guild of Assassins."
}