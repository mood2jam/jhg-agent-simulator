# import local files
from agentconnection import AgentConnection
from agentsimulator import AgentSimulator
from agentutils import agentdecorators
from agentutils import jhgutils

# import packages
import argparse
import time
import numpy as np

def basic_agent_factory(is_simulated=False):
    ConnectionClass = None
    if is_simulated:
        AgentType = AgentSimulator
    else:
        AgentType = AgentConnection

    class BasicAgent(AgentType):
        def __init__(self, code, base_url='http://www.juniorhighgame.com', 
                agent_name='ai', is_player=True, is_bot=True, bot_role=None, 
                bot_reg_key=None):
            super().__init__(code, base_url, agent_name, is_player, 
                is_bot, bot_role, bot_reg_key)

        def play_round(self):
            # How to get the various graphs as data structures see README
            line_data = self.get_line_graph()
            tornado_data = self.get_tornado_graph()
            tornado_matrix = self.get_tornado_graph(matrix_format=True)
            network_data = self.get_network_graph()
            # Uncomment these prints to see the graph data
            '''print("Round data: {}\n".format(self.round_data[self.round_num]))
            print("Line graph: {}\n".format(line_data))
            print("Tornado graph: {}\n".format(tornado_data))
            print("Tornado matrix: {}\n".format(tornado_matrix))
            print("Network graph: {}\n".format(network_data))'''
            
            # If chat is enabled you can send messages as follows
            if self.chat_type == jhgutils.chat_type['free']:
                self.send_message("2 tkns to everyone")
            elif self.chat_type == jhgutils.chat_type['anonymous']:
                self.send_message(f"2 tkns to everyone -{self.player_name}")

            round_transaction = [
                {
                    "receiverName": player, 
                    "amount": 2
                } for player in self.player_list
            ]
            self.submit_tokens(round_transaction)
        
        def new_message(self, message):
            # This will be triggered by an event and thus in a different
            # thread from play_round but it shares access to the memory
            # message is a dictionary with the sender name as 'playerName'
            # and the message as 'body'
            print("{playerName} says '{body}'".format(**message))

    return BasicAgent
            
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c", "--code", type=str, required=True, 
        help="code for the game to join"
    )

    args = parser.parse_args()

    BasicAgentOnline = basic_agent_factory(is_simulated=False)

    bot = BasicAgentOnline(args.code, base_url='http://www.juniorhighgame.com', 
        agent_name='ai', is_player=True, is_bot=False)
    
    bot.wait()